import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/screen/common/web_view_screen.dart';
import 'package:flutter_app/crypto/screen/splash_screen.dart';
import 'package:flutter_app/crypto/widget/theme_switcher.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:flutter_app/localization/localization.dart';
import 'package:flutter_app/crypto/screen/cryptodetails/details_screen.dart';
import 'package:flutter_app/crypto/screen/cryptofavourites/favourite_list_screen.dart';
import 'package:flutter_app/crypto/screen/home_screen.dart';

class CryptoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Cryppy",
      theme: ThemeSwitcher.of(context),
      home: SplashScreen(),
      localizationsDelegates: [
        const LocalizationDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
//        const Locale('ru'),
      ],
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case HomeScreen.route:
            return HomeScreen.getRoute();
          case WebViewScreen.route:
            return WebViewScreen.getRoute(settings);
          case FavouriteListScreen.route:
            return FavouriteListScreen.getRoute();
          case DetailsCryptoScreen.route:
            return DetailsCryptoScreen.getRoute(settings);
          default:
            throw Exception("Invalid route");
        }
      },
    );
  }
}
