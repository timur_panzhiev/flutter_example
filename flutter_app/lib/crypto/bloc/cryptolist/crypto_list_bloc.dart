import 'dart:convert';
import 'dart:io';

import 'package:flutter_app/crypto/bloc/cryptolist/state/crypto_list_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';

import 'event/crypto_list_event.dart';
import 'package:http/http.dart' show Client;

class CryptoBloc extends Bloc<CryptoListEvent, CryptoListState> {

  static const int _INITIAL_CRYPTO_AMOUNT = 50;
  static const int _PAGINATED_CRYPTO_AMOUNT = 20;

  Client client = Client();
  static const _apiKeyName = 'X-CMC_PRO_API_KEY';
  static const _apiKeyValue = '0db5ad1a-4920-4ec2-9130-a5dce5497f10';

  @override
  CryptoListState get initialState => CryptosUninitializedState();

  @override
  Stream<CryptoListState> mapEventToState(CryptoListEvent event) async* {
    if (event is FetchCryptosEvent && !_hasReachedMax(currentState)) {
      try {
        if (currentState is CryptosUninitializedState) {
          final cryptos = await _fetchCryptos(1, _INITIAL_CRYPTO_AMOUNT);
          yield CryptosLoadedState(cryptos: cryptos, hasReachedMax: false);
        }
        if (currentState is CryptosLoadedState) {
          final cryptos =
              await _fetchCryptos((currentState as CryptosLoadedState).cryptos.length + 1, _PAGINATED_CRYPTO_AMOUNT);
          yield cryptos.isEmpty
              ? (currentState as CryptosLoadedState).copyWith(hasReachedMax: true)
              : CryptosLoadedState(
            cryptos: (currentState as CryptosLoadedState).cryptos + cryptos,
            hasReachedMax: false,
          );
        }
      } catch (_) {
        yield CryptosErrorState();
      }
    }

    if (event is FetchInitialCryptosEvent) {
      try {
        final cryptos = await _fetchCryptos(1, _INITIAL_CRYPTO_AMOUNT);
        yield CryptosRefreshingState();
        yield CryptosLoadedState(cryptos: cryptos, hasReachedMax: false);
      } catch (_) {
        yield CryptosErrorState();
      }
    }
  }

  bool _hasReachedMax(CryptoListState state) =>
      state is CryptosLoadedState && state.hasReachedMax;

  Future<List<Crypto>> _fetchCryptos(int start, int limit) async {
    try {
      final response = await client.get(
          'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=$start&limit=$limit',
          headers: {_apiKeyName: _apiKeyValue, 'Accept': 'application/json'});
      if (response.statusCode >= 200) {
        print(json.decode(response.body));
        Map data = json.decode(response.body);
        return (data['data'] as List)
            .map((crypto) => Crypto.fromJson(crypto))
            .toList();
      } else {
        throw Exception('Error');
      }
    } catch (e, stacktrace) {
      print(stacktrace);
      throw Exception('Error');
    }
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    print(stacktrace);
    super.onError(error, stacktrace);
  }
}