import 'package:equatable/equatable.dart';

abstract class CryptoListEvent extends Equatable {}

class FetchCryptosEvent extends CryptoListEvent {

  @override
  String toString() => 'FetchCryptos';
}

class FetchInitialCryptosEvent extends CryptoListEvent {
  @override
  String toString() => 'FetchInitialCryptos';
}