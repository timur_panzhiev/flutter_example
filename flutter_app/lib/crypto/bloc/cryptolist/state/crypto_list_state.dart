import 'package:equatable/equatable.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:meta/meta.dart';

abstract class CryptoListState extends Equatable {
  CryptoListState([List props = const []]) : super(props);
}

class CryptosUninitializedState extends CryptoListState {
  @override
  String toString() => 'CryptosUninitializedState';
}

class CryptosErrorState extends CryptoListState {
  @override
  String toString() => 'CryptosError';
}

class CryptosRefreshingState extends CryptosLoadedState {

  @override
  String toString() => 'CryptosRefreshing';

  CryptosRefreshingState() : super(cryptos: [], hasReachedMax: false);
}

class CryptosLoadedState extends CryptoListState {
  final List<Crypto> cryptos;
  final bool hasReachedMax;

  CryptosLoadedState({
    @required this.cryptos,
    @required this.hasReachedMax,
  }) : super([cryptos, hasReachedMax]);

  CryptosLoadedState copyWith({
    List<Crypto> cryptos,
    bool hasReachedMax,
  }) {
    return CryptosLoadedState(
      cryptos: cryptos ?? this.cryptos,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() =>
      'CryptosLoadedState { cryptos: ${cryptos.length}, hasReachedMax: $hasReachedMax }';
}
