import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_app/crypto/bloc/details/common/state/details_common_state.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:http/http.dart' show Client;

import 'event/details_common_event.dart';

class DetailsCommonBloc extends Bloc<DetailsCommonEvent, DetailsCommonState> {

  Client client = Client();
  static const _apiKeyName = 'X-CMC_PRO_API_KEY';
  static const _apiKeyValue = '0db5ad1a-4920-4ec2-9130-a5dce5497f10';

  @override
  DetailsCommonState get initialState => CryptoLoadingState();

  @override
  Stream<DetailsCommonState> mapEventToState(DetailsCommonEvent event) async* {

    if (event is GetDetailsEvent) {
      yield CryptoLoadedState(event.crypto);
    } else if (event is UpdateDetailsEvent) {
      try {
        final Crypto crypto = await _fetchCrypto(event.symbol);
        yield CommonDetailsRefreshingState();
        yield CryptoLoadedState(crypto);
      } catch (_, stacktrace) {
        print(stacktrace);
        yield CryptoErrorState(crypto: (currentState as CryptoLoadedState).crypto);
      }
    }
  }

  Future<Crypto> _fetchCrypto(String symbol) async {
    try {
      final response = await client.get(
          'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=$symbol',
          headers: {_apiKeyName: _apiKeyValue});
      if (response.statusCode >= 200) {
        Map data = json.decode(response.body);
        return (data['data'] as Map<String, dynamic>)
            .values
            .map((crypto) => Crypto.fromJson(crypto))
            .first;
      } else {
        throw Exception('Error');
      }
    } catch (e, stacktrace) {
      print(stacktrace);
      throw Exception('Error');
    }
  }
}