import 'package:equatable/equatable.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';

abstract class DetailsCommonEvent extends Equatable {}

class GetDetailsEvent extends DetailsCommonEvent {

  final Crypto crypto;
  GetDetailsEvent(this.crypto);

  @override
  String toString() => 'GetDetailsEvent';
}

class UpdateDetailsEvent extends DetailsCommonEvent {

  final String symbol;
  UpdateDetailsEvent(this.symbol);

  @override
  String toString() => 'UpdateDetailsEvent';
}