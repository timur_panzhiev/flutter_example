import 'package:equatable/equatable.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:meta/meta.dart';

abstract class DetailsCommonState extends Equatable {
  DetailsCommonState([List props = const []]) : super(props);
}

class CryptoLoadingState extends DetailsCommonState {
  @override
  String toString() => 'CryptoLoadingState';
}

class CryptoLoadedState extends DetailsCommonState {
  final Crypto crypto;
  CryptoLoadedState(this.crypto) : super([crypto]);

  CryptoLoadedState copyWith(Crypto crypto) {
    return CryptoLoadedState(crypto ?? this.crypto);
  }

  @override
  String toString() =>
      'CryptoLoadedState { crypto: $crypto }';
}

class CommonDetailsRefreshingState extends CryptoLoadedState {
  @override
  String toString() => 'CommonDetailsRefreshingState';

  CommonDetailsRefreshingState() : super(null);
}

class CryptoErrorState extends DetailsCommonState {
  final Crypto crypto;

  CryptoErrorState({
    @required this.crypto,
  }) : super([crypto]);

  @override
  String toString() => 'CryptoErrorState';
}
