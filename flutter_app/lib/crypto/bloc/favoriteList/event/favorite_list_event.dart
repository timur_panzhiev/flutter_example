import 'package:equatable/equatable.dart';

abstract class FavoriteCryptoListEvent extends Equatable {}

class FetchFavoriteCryptosEvent extends FavoriteCryptoListEvent {
  @override
  String toString() => 'FetchFavoriteCryptos';
}