import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:flutter_app/crypto/bloc/favoriteList/event/favorite_list_event.dart';
import 'package:flutter_app/crypto/bloc/favoriteList/state/favorite_list_state.dart';
import 'package:flutter_app/crypto/data/storage/storage.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';

import 'package:http/http.dart' show Client;

class FavoriteCryptoBloc extends Bloc<FavoriteCryptoListEvent, FavoriteCryptoListState> {

  Client client = Client();
  final _apiKeyName = 'X-CMC_PRO_API_KEY';
  final _apiKeyValue = '0db5ad1a-4920-4ec2-9130-a5dce5497f10';

  @override
  FavoriteCryptoListState get initialState => FavoriteCryptosUninitialized();

  @override
  Stream<FavoriteCryptoListState> mapEventToState(FavoriteCryptoListEvent event) async* {
    if (event is FetchFavoriteCryptosEvent) {
      try {
        yield FavoriteCryptosLoaded(cryptos: await _fetchFavoriteCryptos());
      } catch (_) {
        yield FavoriteCryptosError();
      }
    }
  }

  Future<List<Crypto>> _fetchFavoriteCryptos() async {

    List<String> favList = await Storage().getFavorites();
    if (favList == null || favList.isEmpty) {
      return [];
    }

    final response = await client.get(
        'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=${favList
            .join(',')}',
        headers: {_apiKeyName: _apiKeyValue, 'Accept': 'application/json'});
    if (response.statusCode >= 200) {
      print(json.decode(response.body));
      Map data = json.decode(response.body);
      return (data['data'] as Map<String, dynamic>).values
          .map((crypto) => Crypto.fromJson(crypto))
          .toList();
    } else {
      throw Exception('Error');
    }
  }
}