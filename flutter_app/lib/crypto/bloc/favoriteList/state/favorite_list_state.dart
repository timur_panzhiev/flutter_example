import 'package:equatable/equatable.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';

abstract class FavoriteCryptoListState extends Equatable {
  FavoriteCryptoListState([List props = const []]) : super(props);
}

class FavoriteCryptosUninitialized extends FavoriteCryptoListState {
  @override
  String toString() => 'CryptosUninitialized';
}

class FavoriteCryptosError extends FavoriteCryptoListState {
  @override
  String toString() => 'CryptosError';
}

class FavoriteCryptosLoaded extends FavoriteCryptoListState {
  final List<Crypto> cryptos;

  FavoriteCryptosLoaded({
    this.cryptos,
  }) : super([cryptos, FavoriteCryptoListState]);

  FavoriteCryptosLoaded copyWith({
    List<Crypto> cryptos,
    bool hasReachedMax,
  }) {
    return FavoriteCryptosLoaded(
      cryptos: cryptos ?? this.cryptos
    );
  }

  @override
  String toString() =>
      'CryptosLoaded { cryptos: ${cryptos.length} }';
}