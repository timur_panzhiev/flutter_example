import 'package:equatable/equatable.dart';

abstract class NewsEvent extends Equatable {}

class LatestNewsEvent extends NewsEvent {
  @override
  String toString() => 'LatestNewsEvent';
}