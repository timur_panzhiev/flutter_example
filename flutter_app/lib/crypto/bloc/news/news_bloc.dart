import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_app/crypto/bloc/news/state/news_state.dart';
import 'package:flutter_app/crypto/models/news_model.dart';
import 'package:http/http.dart' show Client;

import 'event/news_event.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {

  Client client = Client();
  final _apiKeyNewsName = 'x-api-key';
  final _apiKeyNewsValue = '1a18884294ccbfc377590660983a11ed';

  @override
  NewsState get initialState => NewsLoadingState();

  @override
  Stream<NewsState> mapEventToState(NewsEvent event) async* {

    if (event is LatestNewsEvent) {
      try {
        final List<News> news = await _fetchNews();
        yield NewsRefreshingState();
        yield NewsLoadedState(news: news);
      } catch (_) {
        yield NewsErrorState();
      }
    }
  }

  Future<List<News>> _fetchNews() async {
    try {
      final response = await client.get(
          'https://cryptocontrol.io/api/v1/public/news/',
          headers: {_apiKeyNewsName: _apiKeyNewsValue});
      if (response.statusCode >= 200) {
        print(json.decode(response.body));
        List data = json.decode(response.body);
        return data
            .map((news) => News.fromJson(news))
            .toList();
      } else {
        throw Exception('error fetching events');
      }
    } catch (_) {
      throw Exception('error fetching events');
    }
  }
}