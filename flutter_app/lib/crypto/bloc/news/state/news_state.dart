import 'package:equatable/equatable.dart';
import 'package:flutter_app/crypto/models/news_model.dart';
import 'package:meta/meta.dart';

abstract class NewsState extends Equatable {
  NewsState([List props = const []]) : super(props);
}

class NewsLoadingState extends NewsState {
  @override
  String toString() => 'NewsLoadingState';
}

class NewsErrorState extends NewsState {
  @override
  String toString() => 'NewsErrorState';
}

class NewsLoadedState extends NewsState {
  final List<News> news;

  NewsLoadedState({
    @required this.news
  }) : super([news]);

  NewsLoadedState copyWith({
    List<News> news
  }) {
    return NewsLoadedState(
      news: news ?? this.news,
    );
  }

  @override
  String toString() =>
      'NewsLoadedState { news: ${news.length} }';
}

class NewsRefreshingState extends NewsLoadedState {

  @override
  String toString() => 'NewsRefreshingState';

  NewsRefreshingState() : super(news: []);
}
