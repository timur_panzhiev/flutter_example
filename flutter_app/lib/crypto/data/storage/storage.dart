import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage implements Storage {
  LocalStorage._internal() {
    SharedPreferences.getInstance().then((preferences) {
      _preferences = preferences;
      _completer.complete(preferences);
    });
  }

  static const _NIGHT_MODE_KEY = 'night_mode_key';
  static const _FAV_LIST = '_fav_list';

  SharedPreferences _preferences;
  final _completer = Completer<SharedPreferences>();

  @override
  Future<bool> isNightTheme() async =>
      _operationExecutor<bool>((preferences) => preferences.getBool(_NIGHT_MODE_KEY));

  @override
  void saveThemeMode(bool isNightMode) {
    _operationExecutor((preferences) {
      preferences.setBool(_NIGHT_MODE_KEY, isNightMode);
    });
  }

  @override
  void setFavorite(bool isFavorite, String symbol) {
    _operationExecutor((preferences) {
      List<String> favList = preferences.getStringList(_FAV_LIST);
      if (favList == null) {
        favList = [];
      }
      favList.remove(symbol);
      if (isFavorite) {
        favList.add(symbol);
      }
      preferences.setStringList(_FAV_LIST, favList);
    });
  }

  @override
  Future<List<String>> getFavorites() =>
      _operationExecutor<List<String>>((preferences) => preferences.getStringList(_FAV_LIST));

  @override
  Future<bool> isFavorite(String symbol) {
    return _operationExecutor<bool>((preferences) {
      List<String> favList = preferences.getStringList(_FAV_LIST);
      if (favList == null) {
        return false;
      }
      return favList.contains(symbol);
    });
  }

  @override
  void clean() {
    _operationExecutor((prefs) {
      prefs.clear();
    });
  }

  Future<T> _operationExecutor<T>(T Function(SharedPreferences prefs) apply) async {
    T result;
    if (_completer.isCompleted) {
      result = apply(_preferences);
    } else {
      final sharedPreferences = await _completer.future;
      result = apply(sharedPreferences);
    }
    return result;
  }
}

abstract class Storage {
  factory Storage() {
    return LocalStorage._internal();
  }

  Future<bool> isNightTheme();

  void saveThemeMode(bool isNightTheme);

  void setFavorite(bool isFavorite, String symbol);

  Future<List<String>> getFavorites();

  Future<bool> isFavorite(String symbol);

  void clean();
}

abstract class StorageSerializer {
  void serialize(SharedPreferences prefs);
}
