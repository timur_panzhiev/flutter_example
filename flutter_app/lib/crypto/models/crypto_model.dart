import 'package:equatable/equatable.dart';

const String baseImageUrl =
    'https://s2.coinmarketcap.com/static/img/coins/128x128/';

class Crypto extends Equatable {
  final String symbol, name, imageUrl, lastUpdated, dateAdded;
  final double price, volume24h, percentChange1h, percentChange24h, percentChange7d, marketCap;
  final int rank;
  final num circulatingSupply, totalSupply, maxSupply;

  Crypto({this.symbol,
    this.name,
    this.rank,
    this.circulatingSupply,
    this.totalSupply,
    this.maxSupply,
    this.lastUpdated,
    this.dateAdded,
    this.imageUrl,
    this.price,
    this.volume24h,
    this.percentChange1h,
    this.percentChange24h,
    this.percentChange7d,
    this.marketCap})
      : super(
      [
        symbol,
        name,
        rank,
        circulatingSupply,
        totalSupply,
        maxSupply,
        lastUpdated,
        dateAdded,
        imageUrl,
        price,
        volume24h,
        percentChange1h,
        percentChange24h,
        percentChange7d,
        marketCap
      ]
  );

  factory Crypto.fromJson(Map<String, dynamic> data) {
    Map<String, dynamic> quoteMap = data['quote'];
    Map<String, dynamic> quoteEntity = quoteMap['USD'];
    return Crypto(
        symbol: data['symbol'],
        name: data['name'],
        rank: data['cmc_rank'],
        circulatingSupply: data['circulating_supply'],
        totalSupply: data['total_supply'],
        maxSupply: data['max_supply'],
        lastUpdated: data['last_updated'],
        dateAdded: data['date_added'],
        imageUrl: "$baseImageUrl${data['id']}.png",
        price: quoteEntity['price'],
        volume24h: quoteEntity['volume_24h'],
        percentChange1h: quoteEntity['percent_change_1h'],
        percentChange24h: quoteEntity['percent_change_24h'],
        percentChange7d: quoteEntity['percent_change_7d'],
        marketCap: quoteEntity['market_cap']);
  }
}