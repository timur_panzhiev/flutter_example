import 'package:equatable/equatable.dart';

class News extends Equatable {
  String id, primaryCategory, description, publishedAt, title, url = '',
      sourceDomain, thumbnailUrl;

  News({
    this.id,
    this.primaryCategory,
    this.description,
    this.publishedAt,
    this.title,
    this.url,
    this.sourceDomain,
    this.thumbnailUrl
  }) : super([
    id,
    primaryCategory,
    description,
    publishedAt,
    title,
    url,
    sourceDomain,
    thumbnailUrl
  ]);

  factory News.fromJson(Map<String, dynamic> data) {
    return News(
        id: data['_id'],
        primaryCategory: data['primaryCategory'],
        description: data['description'],
        publishedAt: data['publishedAt'],
        title: data['title'],
        url: data['url'],
        sourceDomain: data['sourceDomain'],
        thumbnailUrl: data['thumbnail'] ?? '');
  }
}
