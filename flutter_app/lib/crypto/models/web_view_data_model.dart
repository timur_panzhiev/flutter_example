class WebViewDataModel {
  final String appBarTitle, url;
  WebViewDataModel(this.appBarTitle, this.url);
}