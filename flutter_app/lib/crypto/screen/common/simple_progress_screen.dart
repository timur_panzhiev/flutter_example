import 'package:flutter/material.dart';

class SimpleProgressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: CircularProgressIndicator(strokeWidth: 1));
  }
}
