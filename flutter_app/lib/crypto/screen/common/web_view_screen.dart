import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/models/web_view_data_model.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  static const String route = "/webViewScreen";

  static Route getRoute(RouteSettings settings) =>
      MaterialPageRoute(builder: (context) => WebViewScreen(key: Key(settings.arguments.toString()), data: settings.arguments));

  final WebViewDataModel data;
  WebViewScreen({Key key, @required this.data}) : super(key: key);

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {

  WebViewController _controller;
  bool isProgressVisible = true;

  @override
  Widget build(BuildContext context) {
    return getScreen();
  }

  getScreen() =>
      WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
            backgroundColor: Theme
                .of(context)
                .backgroundColor,
            appBar: AppBar(
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    setState(() {
                      _controller.reload();
                      isProgressVisible = true;
                    });
                  },
                ),
              ],
              title: Text(widget.data.appBarTitle),
            ),
            body: getBody()),
      );

  Widget getBody() {
    return Stack(
      children: <Widget>[
        WebView(
          onPageFinished: (_) {
            setState(() {
              isProgressVisible = false;
            });
          },
          navigationDelegate: (navigationRequest) {
            print(navigationRequest.url);
            return NavigationDecision.navigate;
          },
          onWebViewCreated: (WebViewController c) {
            _controller = c;
          },
          initialUrl: widget.data.url,
          javascriptMode: JavascriptMode.unrestricted,
        ),
        Visibility(
            visible: isProgressVisible,
            child: Center(
              child: CircularProgressIndicator(),
            ))
      ],
    );
  }

  Future<bool> _onWillPop() async {
    if (await _controller.canGoBack()) {
      _controller.goBack();
      return false;
    }
    return true;
  }
}
