import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/bloc/details/common/details_common_bloc.dart';
import 'package:flutter_app/crypto/bloc/details/common/event/details_common_event.dart';
import 'package:flutter_app/crypto/bloc/details/common/state/details_common_state.dart';
import 'package:flutter_app/crypto/data/storage/storage.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:flutter_app/crypto/screen/common/simple_progress_screen.dart';
import 'package:flutter_app/crypto/utils/formatter.dart';
import 'package:flutter_app/crypto/utils/hero_tags.dart';

import 'package:flutter_app/localization/localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailsCryptoScreen extends StatefulWidget {
  static const String route = '/detailsCryptoScreen';
//  static Route getRoute(RouteSettings settings) =>
//      MaterialPageRoute(builder: (context) => DetailsCryptoScreen(settings.arguments));

  static Route getRoute(RouteSettings settings) =>
      MaterialPageRoute(builder: (context) =>
          BlocProvider(
            builder: (_) =>
            DetailsCommonBloc()
              ..dispatch(GetDetailsEvent(settings.arguments)),
            child: DetailsCryptoScreen(),
          )
      );

  final Storage storage = Storage();

  @override
  _DetailsCryptoScreenState createState() => _DetailsCryptoScreenState();
}

class _DetailsCryptoScreenState extends State<DetailsCryptoScreen> {
  ScrollController _scrollController;
  final double expandedAppBarHeight = 200;
   bool isFavourite = false;
  DetailsCommonBloc _detailsCommonBloc;

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(() => setState(() {}));
    _detailsCommonBloc = BlocProvider.of<DetailsCommonBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(body: _getBlocBuilder());

  Widget _getBlocBuilder() {
    return BlocBuilder(
      bloc: _detailsCommonBloc,
      builder: (_, state) {
        if (state is CryptoLoadedState) {
          return _getBody(state.crypto);
        } else if (state is CryptoErrorState) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Something went wrong')));
          return _getBody(state.crypto);
        } else if (state is CryptoLoadingState) {
          return SimpleProgressScreen();
        }
      },
    );
  }

  Widget _getBody(Crypto crypto) {
    return Scaffold(
        body: DefaultTabController(
          length: 4,
          child: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  _getSliverAppBar(crypto),
                  _getTabBar()
                ];
              },
              body: _getTabBarContent()
          ),
        )
    );
  }


  Widget _getSliverAppBar(Crypto crypto) =>
      SliverAppBar(
          title: _getAppBarTitle(crypto),
          backgroundColor: Colors.green,
          floating: false,
          actions: <Widget>[
            FutureBuilder(
                future: Storage().isFavorite(crypto.symbol),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    isFavourite = snapshot.data;
                    return IconButton(
                      onPressed: () {
                        setState(() {
                          isFavourite = !isFavourite;
                          widget.storage.setFavorite(
                              isFavourite, crypto.symbol);
                        });
                      },
                      icon: IconTheme(
                          data: Theme
                              .of(context)
                              .iconTheme,
                          child: Icon(
                              isFavourite ? Icons.star : Icons.star_border)),
                    );
                  } else {
                    return SimpleProgressScreen();
                  }
                }),
          ],
          pinned: true,
          expandedHeight: expandedAppBarHeight,
          flexibleSpace: getFlexibleAppBarContent(crypto)
      );

  Widget _getAppBarTitle(Crypto crypto) {
    // expanded appbar height - appbar height (200 - 56)
    final double flexibleHeight = 144;
    double opacity = 0;

    if (_scrollController.hasClients) {
      double offset = _scrollController.offset;
      if (offset > flexibleHeight){
        opacity = 1;
      } else {
        opacity = offset / flexibleHeight;
      }
    }

    return Opacity(
      opacity: opacity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Image.network(crypto.imageUrl, width: 24, height: 24),
          ),
          Expanded(child: Text(crypto.name, overflow: TextOverflow.fade,))
        ],
      ),
    );
  }

  Widget getFlexibleAppBarContent(Crypto crypto){
    // expanded appbar height - appbar height (200 - 56)
    final double flexibleHeight = 144;

    double startOpacity = 1.0;
    double opacity = 1.0;

    if (_scrollController.hasClients) {
      double offset = _scrollController.offset;
      if (offset > flexibleHeight){
        opacity = 0;
      } else if (offset == 0) {
        opacity = startOpacity - offset / flexibleHeight;
      } else {
        opacity = startOpacity / 2 - offset / flexibleHeight;
        opacity = opacity.isNegative ? 0 : opacity;
      }
    }
    return Opacity(
      opacity: opacity,
      child: Center(
        child: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.only(top: 16),
              color: Colors.green,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Hero(
                      tag: crypto.imageUrl,
                      child: Image.network(crypto.imageUrl, width: 50, height: 50)
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(crypto.symbol,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            )
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 4, right: 4),
                          color: Colors.white.withOpacity(0.5),
                          width: 2,
                          height: 25,
                        ),
                        Text(crypto.name,
                            style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.white
                            )
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Text(
                      '\$ ${Formatter.getFormattedAmount(crypto.price)}',
                      style: TextStyle(fontSize: 25, color: Colors.white),),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      '\$ ${Formatter.getFormattedAmount(crypto.marketCap)}',
                      style: TextStyle(fontSize: 14, color: Colors.white),),
                  )
                ],
              )
          ),
        ),
      ),
    );
  }

  Widget _getTabBar() =>
      SliverPersistentHeader(
        delegate: _SliverAppBarDelegate(
          TabBar(
            indicatorSize: TabBarIndicatorSize.label,
            labelPadding: EdgeInsets.all(0),
            labelColor: Colors.green,
            unselectedLabelColor: Colors.black26,
            tabs: _getTabs(),
          ),
        ),
        pinned: true,
      );

  List<Widget> _getTabs() =>
      [
        Tooltip(
          message: S.of(context).commonInfo,
          child: Tab(icon: Icon(Icons.info)),
        ),
        Tooltip(
          message: S.of(context).historicalData,
          child: Tab(icon: Icon(Icons.history)),
        ),
        Tooltip(
          message: S.of(context).exchanges,
          child: Tab(icon: Icon(Icons.insert_chart)),
        ),
        Tooltip(
          message: S.of(context).news,
          child: Tab(icon: Icon(Icons.public)),
        ),
      ];

  Widget _getTabBarContent() =>
      TabBarView(
        children: <Widget>[
          Center(child: Text(S.of(context).commonInfo)),
          Center(child: Text(S.of(context).historicalData)),
          Center(child: Text(S.of(context).exchanges)),
          Center(child: Text(S.of(context).news)),
        ],
      );
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar _tabBar;

  _SliverAppBarDelegate(this._tabBar);

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(BuildContext context, double shrinkOffset,
      bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

//  Widget _getFab() {
//    //starting fab position
//    final double defaultTopMargin = expandedAppBarHeight - 8.0;
//    //pixels from top where scaling should start
//    final double scaleStart = 96.0;
//    //pixels from top where scaling should end
//    final double scaleEnd = scaleStart / 2;
//
//    double top = defaultTopMargin;
//    double scale = 1.0;
//    if (_scrollController.hasClients) {
//      double offset = _scrollController.offset;
//      top -= offset;
//      if (offset < defaultTopMargin - scaleStart) {
//        //offset small => don't scale down
//        scale = 1.0;
//      } else if (offset < defaultTopMargin - scaleEnd) {
//        //offset between scaleStart and scaleEnd => scale down
//        scale = (defaultTopMargin - scaleEnd - offset) / scaleEnd;
//      } else {
//        //offset passed scaleEnd => hide fab
//        scale = 0.0;
//      }
//    }
//    return Positioned(
//      top: top,
//      right: 16.0,
//      child: Transform(
//        transform: Matrix4.identity()..scale(scale),
//        alignment: Alignment.center,
//          child: FutureBuilder(
//              future: Storage().isFavorite(widget.crypto.symbol),
//              builder: (context, snapshot) {
//                if (snapshot.hasData) {
//                  isFavourite = snapshot.data;
//                  return FloatingActionButton(
//                    backgroundColor: Colors.orange,
//                    onPressed: () {
//                      setState(() {
//                        isFavourite = !isFavourite;
//                        widget.storage.setFavorite(
//                            isFavourite, widget.crypto.symbol);
//                      });
//                    },
//                    child: IconTheme(
//                        data: Theme
//                            .of(context)
//                            .iconTheme,
//                        child: Icon(
//                            isFavourite ? Icons.star : Icons.star_border)),
//                  );
//                } else {
//                  return Center(child: CircularProgressIndicator());
//                }
//              })
//      ),
//      );
//  }
