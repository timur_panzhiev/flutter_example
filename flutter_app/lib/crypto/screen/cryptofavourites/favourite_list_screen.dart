import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/bloc/favoriteList/event/favorite_list_event.dart';
import 'package:flutter_app/crypto/bloc/favoriteList/favorite_crypto_list_bloc.dart';
import 'package:flutter_app/crypto/bloc/favoriteList/state/favorite_list_state.dart';
import 'package:flutter_app/crypto/data/storage/storage.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:flutter_app/crypto/screen/cryptodetails/details_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_app/localization/localization.dart';

class FavouriteListScreen extends StatefulWidget {
  static const String route = '/favouriteListScreen';
  static const String TAG = "FavouriteListScreen";
  final Storage storage = Storage();

  static Route getRoute() =>
      MaterialPageRoute(builder: (context) =>
          BlocProvider(
            builder: (_) =>
            FavoriteCryptoBloc()
              ..dispatch(FetchFavoriteCryptosEvent()),
            child: FavouriteListScreen(),
          )
      );

  @override
  _FavouriteListScreenState createState() => _FavouriteListScreenState();
}

class _FavouriteListScreenState extends State<FavouriteListScreen> {

  FavoriteCryptoBloc _favoriteCryptoBloc;

  @override
  void initState() {
    _favoriteCryptoBloc = BlocProvider.of<FavoriteCryptoBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .backgroundColor,
        appBar: AppBar(
          title: Text(S
              .of(context)
              .favouritesTitle,
              style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        body: getBody());
  }

  Widget getBody () {
    return BlocBuilder(
      bloc: _favoriteCryptoBloc,
      builder: (context, state) {
        if (state is FavoriteCryptosUninitialized) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is FavoriteCryptosError) {
          return Center(
            child: Text('Failed to fetch cryptos'),
          );
        } else if (state is FavoriteCryptosLoaded) {
          return _getGrid(state.cryptos);
        }
      },
    );
  }

  Widget _getGrid(List<Crypto> cryptos){
    if (cryptos.isEmpty) {
      return Center(
        child: Text('You have no favorite cryptocurrencies'),
      );
    } else {
      return GridView.builder(
        itemBuilder: (context, position) {
          return _getGridCell(cryptos[position]);
        },
        itemCount: cryptos.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3),
      );
    }
  }

  _getGridCell(Crypto crypto) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(
              DetailsCryptoScreen.route, arguments: crypto);
        },
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Image.network(
                  crypto.imageUrl,
                  height: 24,
                  width: 24,
                ),
                Text(crypto.symbol),
                Text(crypto.name),
              ],
            ),
          ),
        ),
      ),
    );
  }
}