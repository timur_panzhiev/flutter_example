import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/bloc/cryptolist/crypto_list_bloc.dart';
import 'package:flutter_app/crypto/bloc/cryptolist/event/crypto_list_event.dart';
import 'package:flutter_app/crypto/bloc/cryptolist/state/crypto_list_state.dart';
import 'package:flutter_app/crypto/models/crypto_model.dart';
import 'package:flutter_app/crypto/screen/cryptodetails/details_screen.dart';
import 'package:flutter_app/crypto/utils/formatter.dart';
import 'package:flutter_app/crypto/utils/hero_tags.dart';
import 'package:flutter_app/localization/localization.dart';
import 'package:flutter_app/crypto/screen/cryptofavourites/favourite_list_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../styles.dart';

class CryptoListScreen extends StatefulWidget {
  @override
  _CryptoListScreenState createState() => _CryptoListScreenState();
}

class _CryptoListScreenState extends State<CryptoListScreen>
    with AutomaticKeepAliveClientMixin {
  List<Crypto> cryptos = [];
  List<Crypto> filteredList = [];
  bool isSearchMode = false;

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  CryptoBloc _cryptoBloc;
  Completer<void> _refreshCompleter;

  TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
    _scrollController.addListener(_onScroll);
    _cryptoBloc = BlocProvider.of<CryptoBloc>(context);
    _textEditingController = TextEditingController();
    _textEditingController.addListener(_onTextChanged);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: isSearchMode ? _searchModeAppBar() : _nonSearchModeAppBar(),
      body: isSearchMode ? _searchModeBody() : _nonSearchModeBody(),
    );
  }

  _searchModeAppBar(){
    return AppBar(
      title: TextField(
        cursorColor: Styles.cursorColor,
        style: Theme.of(context).textTheme.display1,
        decoration: InputDecoration(
            hintStyle: Theme.of(context).textTheme.display1.copyWith(
              color: Styles.translucentWhite
            ),
            border: InputBorder.none,
            hintText: 'Search'
        ),
        autofocus: true,
        controller: _textEditingController,
      ),
      actions: _searchModeActions(),
    );
  }

  _nonSearchModeAppBar() {
    return AppBar(
      title: Hero(
        tag: HeroTags.HERO_APP_TITLE,
        child: Container(
          alignment: Alignment.centerLeft,
          child: Text(S
              .of(context)
              .appTitle,
            style: Theme
                .of(context)
                .textTheme
                .display4
                .copyWith(
                letterSpacing: 0,
                fontSize: 18
            ),
          ),
        ),
      ),
      actions: _nonSearchModeActions(),
    );
  }

  List<Widget> _searchModeActions() {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          setState(() {
            isSearchMode = !isSearchMode;
          });
        },
      ),
    ];
  }

  List<Widget> _nonSearchModeActions() {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.star),
        onPressed: () {
          Navigator.of(context).pushNamed(FavouriteListScreen.route);
        },
      ),
      BlocBuilder(
          bloc: _cryptoBloc,
          builder: (BuildContext context, CryptoListState state) {
            if (state is CryptosLoadedState) {
              return IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    isSearchMode = !isSearchMode;
                    filteredList = List<Crypto>.from(cryptos);
                    _textEditingController.text = '';
                  });
                },
              );
            } else {
              return Container();
            }
          }
      )
    ];
  }

  Widget _searchModeBody() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return getCryptoItem(filteredList[index]);
      },
      itemCount: filteredList.length,
    );
  }

  Widget _nonSearchModeBody() {
    return BlocBuilder(
      bloc: _cryptoBloc,
      // ignore: missing_return
      builder: (BuildContext context, CryptoListState state) {
        if (state is CryptosUninitializedState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is CryptosErrorState) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Failed to fetch cryptos'),
                IconButton(
                  onPressed: () => getInitialPage(),
                  icon: Icon(Icons.refresh, color: Styles.retryButtonColor),
                )
              ],
            ),
          );
        } else if (state is CryptosLoadedState) {
          cryptos = List<Crypto>.from(state.cryptos);
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
          return RefreshIndicator(
            onRefresh: () {
              getInitialPage();
              return _refreshCompleter.future;
            },
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return index >= state.cryptos.length
                    ? Center(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: CircularProgressIndicator(strokeWidth: 1),
                  ),
                )
                    : getCryptoItem(state.cryptos[index]);
              },
              itemCount: state.hasReachedMax
                  ? state.cryptos.length
                  : state.cryptos.length + 1,
              controller: _scrollController,
            ),
          );
        }
      },
    );
  }

  getCryptoItem(Crypto crypto) {
    return InkWell(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Hero(
                tag: crypto.imageUrl,
                child: Image.network(
                  crypto.imageUrl,
                  height: 24,
                  width: 24,
                ),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          crypto.symbol,
                          style: Theme
                              .of(context)
                              .textTheme
                              .title
                              .copyWith(fontSize: 14),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(crypto.name,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .title
                                  .copyWith(fontSize: 10, fontWeight: FontWeight.normal)),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text("\$ ${Formatter.getFormattedAmount(crypto.price)}",
                        style: Theme
                            .of(context)
                            .textTheme
                            .title
                            .copyWith(fontSize: 14, fontWeight: FontWeight.normal)),
                    Text("\$ ${Formatter.getFormattedAmount(crypto.marketCap)}",
                      style: Theme
                          .of(context)
                          .textTheme
                          .title
                          .copyWith(fontSize: 10, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Center(
                      child: Text(
                        "${crypto.percentChange7d.toStringAsFixed(2)}%",
                        style: TextStyle(color: getPercentColor(crypto.percentChange7d)),
                      )),
                ),
              )
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context)
              .pushNamed(DetailsCryptoScreen.route, arguments: crypto);
        });
  }

  getPercentColor(double percentChange7d) =>
      percentChange7d.isNegative ? Colors.orange[800] : Colors.green;

  getInitialPage() => _cryptoBloc.dispatch(FetchInitialCryptosEvent());

  void _onTextChanged() {
    var input = _textEditingController.text;
    if (input.isNotEmpty){
      setState(() {
        filteredList = cryptos
            .where((crypto) => crypto.symbol.toLowerCase().contains(input) || crypto.name.toLowerCase().contains(input)).toList();
        print('$filteredList');
      });
    }
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _cryptoBloc.dispatch(FetchCryptosEvent());
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
