import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/bloc/cryptolist/crypto_list_bloc.dart';
import 'package:flutter_app/crypto/bloc/cryptolist/event/crypto_list_event.dart';
import 'package:flutter_app/crypto/bloc/news/event/news_event.dart';
import 'package:flutter_app/crypto/bloc/news/news_bloc.dart';
import 'package:flutter_app/crypto/screen/news/news_screen.dart';
import 'package:flutter_app/crypto/screen/settings/settings.dart';

import 'package:flutter_app/crypto/screen/cryptolist/crypto_list_screen.dart';
import 'package:flutter_app/crypto/utils/custom_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  static const String route = "/homeScreen";
  HomeScreen({Key key}) : super(key: key);

  static Route getRoute({RouteSettings settings}) =>
      FadeRoute(builder: (context) => HomeScreen());

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildPageView(),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).bottomAppBarColor,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('Cryptos'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.public),
            title: Text('News'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onBottomItemClick,
      ),
    );
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  Widget buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        onPageChanged(index);
      },
      children: <Widget>[
        BlocProvider(
            builder: (context) =>
            CryptoBloc()
              ..dispatch(FetchInitialCryptosEvent()),
            child: CryptoListScreen()),
        BlocProvider(
            builder: (context) =>
            NewsBloc()
              ..dispatch(LatestNewsEvent()),
            child: NewsScreen()),
        SettingsScreen(),
      ],
    );
  }

  void onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onBottomItemClick(int index) {
    _selectedIndex = index;
    pageController.animateToPage(
        index, duration: Duration(milliseconds: 300), curve: Curves.ease);
  }
}