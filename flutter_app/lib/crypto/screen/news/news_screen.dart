import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/bloc/news/event/news_event.dart';
import 'package:flutter_app/crypto/bloc/news/news_bloc.dart';
import 'package:flutter_app/crypto/bloc/news/state/news_state.dart';
import 'package:flutter_app/crypto/models/news_model.dart';
import 'package:flutter_app/crypto/models/web_view_data_model.dart';
import 'package:flutter_app/crypto/screen/common/simple_progress_screen.dart';
import 'package:flutter_app/crypto/screen/common/web_view_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';


import 'package:flutter_app/localization/localization.dart';
import '../../styles.dart';

class NewsScreen extends StatefulWidget {
  static const String route = '/newsScreen';
  static const String TAG = "NewsScreen";

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen>
    with AutomaticKeepAliveClientMixin {

  NewsBloc _newsBloc;
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _newsBloc = BlocProvider.of<NewsBloc>(context);
    _refreshCompleter = Completer();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .backgroundColor,
        appBar: AppBar(
          title: Text(S
              .of(context)
              .newsTitle),
        ),
        body: getNewsBody()
    );
  }

  Widget getNewsBody() {
    return BlocBuilder(
      bloc: _newsBloc,
      builder: (context, state) {
        if (state is NewsLoadingState) {
          return getLoadingWidget();
        } else if (state is NewsLoadedState) {
          _refreshCompleter.complete();
          _refreshCompleter = Completer();
          return getContentLoadedWidget(state.news);
        } else if (state is NewsErrorState) {
          return getErrorWidget();
        }
      },
    );
  }

  Widget getLoadingWidget() => SimpleProgressScreen();

  Widget getContentLoadedWidget(List<News> news) {
    return RefreshIndicator(
      onRefresh: () {
        _fetchNews();
        return _refreshCompleter.future;
      },
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return getNewsItem(news[index]);
        },
        itemCount: news.length
      ),
    );
  }

  Widget getErrorWidget() =>
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Failed to fetch news'),
            IconButton(
              onPressed: _fetchNews,
              icon: Icon(Icons.refresh, color: Styles.retryButtonColor),
            )
          ],
        ),
      );

  _fetchNews() => _newsBloc.dispatch(LatestNewsEvent());

  @override
  bool get wantKeepAlive => true;

  Widget getNewsItem(News news) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(WebViewScreen.route, arguments: WebViewDataModel(news.title, news.url));
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: <Widget>[
                    Text("${DateFormat.yMMMMd().format(DateTime.parse(news.publishedAt))} | ${news.sourceDomain}", style: Theme
                        .of(context)
                        .textTheme
                        .subtitle
                        .copyWith(fontSize: 10)),
                  ],
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Image.network(news.thumbnailUrl),
                ),
                Expanded(
                  flex: 4,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(news.title, style: Theme
                        .of(context)
                        .textTheme
                        .title
                        .copyWith(fontSize: 14)),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text(news.description, style: Theme
                  .of(context)
                  .textTheme
                  .subtitle
                  .copyWith(fontSize: 12)),
            ),
            Container(
              child: IconButton(
                  color: Styles.accentColor,
                  icon: Icon(Icons.share), onPressed: () {
                    Share.share(news.url);
              }),
            ),
            Container(
              height: 0.5,
              color: Colors.green,
            )
          ],
        ),
      ),
    );
  }
}
