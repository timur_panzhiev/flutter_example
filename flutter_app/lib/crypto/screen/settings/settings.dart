import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/data/storage/storage.dart';

import 'package:flutter_app/crypto/widget/theme_switcher.dart';

class SettingsScreen extends StatefulWidget {
  static const String route = "/settingsScreen";
  static const String TAG = "SettingsScreen";
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with AutomaticKeepAliveClientMixin {
  var _isNightMode = false;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(title: Text("Settings")),
      body: getBody(),
    );
  }

  Widget getBody() {
    return ListView(
      children: <Widget>[
        FutureBuilder(
          future: Storage().isNightTheme(),
          initialData: false,
          builder: (context, snapshot) {
            _isNightMode = snapshot.data ?? false;
            return SwitchListTile(
              title: Text("Night Mode"),
              value: _isNightMode,
              onChanged: _onChangedTheme,
            );
          },
        )
      ],
    );
  }

  void _onChangedTheme(bool isNightMode) {
    setState(() {
      _isNightMode = isNightMode;
      ThemeSwitcher.instanceOf(context).changeTheme(isNightMode);
      Storage().saveThemeMode(isNightMode);
    });
  }

  @override
  bool get wantKeepAlive => true;
}
