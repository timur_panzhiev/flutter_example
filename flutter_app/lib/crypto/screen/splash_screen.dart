import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/utils/hero_tags.dart';

import 'package:flutter_app/localization/localization.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();

    animationController =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(animationController)
      ..addListener(() {
        setState(() {});
      });
    animationController.forward();

    Timer(Duration(seconds: 2),
            () => Navigator.of(context).pushReplacementNamed(HomeScreen.route));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.0, 0.5, 1.0],
                  colors: [
                    Colors.green.withOpacity(0.7),
                    Colors.green.withOpacity(0.85),
                    Colors.green
                  ],
                )),
          ),
          Opacity(
              opacity: animation.value,
              child: Center(
                  child: Hero(
                    tag: HeroTags.HERO_APP_TITLE,
                    child: Text(
                      S.of(context).appTitle.toUpperCase(),
                      style: Theme.of(context).textTheme.display4,
                    ),
                  )))
        ],
      ),
    );
  }

  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}