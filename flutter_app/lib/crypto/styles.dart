import 'package:flutter/material.dart';

class Styles {
  const Styles();

  static TextStyle smallButton(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .button
        .copyWith(fontSize: 12.0, fontWeight: FontWeight.bold);
  }

  static const Radius radius = Radius.circular(4.0);
  
  // Day Mode 
  static const Color primaryColorDay = Colors.green;
  static const Color accentColor = Colors.orange;
  static const Color errorColorDay = Color(0xFFD80027);
  static const Color disabledColorDay = Colors.black38;
  static const Color titleColorDay = Colors.black;
  static const Color appBarTitleColorDay = Colors.white;
  static const Color subtitleColorDay = Colors.black54;
  static const Color backgroundDay = Colors.white;
  static const Color bottomNavBackgroundDay = Colors.white;
  static const _appbarThemeDay =
  AppBarTheme(elevation: 0.0,
      color: primaryColorDay,
      textTheme: TextTheme(
          title: TextStyle(color: appBarTitleColorDay, fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 18)),
      iconTheme: const IconThemeData(color: Colors.white, size: iconSize),
  );

  // Night Mode
  static const Color primaryColorNight = Color(0xFF454545);
  static const Color errorColorNight = Color(0xFFD80027);
  static const Color disabledColorNight = Colors.black38;
  static const Color titleColorNight = Colors.white;
  static const Color appBarTitleColorNight = Colors.black;
  static const Color subtitleColorNight = Colors.white70;
  static const Color backgroundNight = Colors.black54;
  static const Color bottomNavBackgroundNight = Color(0xFF454545);
  static const _appbarThemeNight =
  AppBarTheme(elevation: 0.0,
    color: primaryColorNight,
    textTheme: TextTheme(
        title: TextStyle(color: appBarTitleColorNight, fontFamily: 'Montserrat', fontWeight: FontWeight.bold, fontSize: 18)),
    iconTheme: const IconThemeData(color: Colors.black, size: iconSize),
  );

  // Common colors
  static const Color retryButtonColor = Colors.green;
  static const Color cursorColor = Colors.orange;
  static const Color translucentWhite = Color(0x50ffffff);

  static const double iconSize = 24;

  static Color listHeadingColor = const Color(0x6009292F);

  ThemeData get appThemeDataDay =>
      ThemeData(
          bottomAppBarColor: bottomNavBackgroundDay,
          primaryColorBrightness: Brightness.dark,
          primaryColor: primaryColorDay,
          accentColor: accentColor,
          backgroundColor: backgroundDay,
          buttonColor: accentColor,
          errorColor: errorColorDay,
          disabledColor: disabledColorDay,
          toggleableActiveColor: primaryColorDay,
          fontFamily: 'Montserrat',
          iconTheme: const IconThemeData(color: Colors.white, size: iconSize),
          textTheme: const TextTheme(
              display1: TextStyle(
                  color: appBarTitleColorDay,
                  fontSize: 18),
              display4: TextStyle(
                  letterSpacing: 10,
                  color: appBarTitleColorDay,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
              title: TextStyle(
                  color: titleColorDay,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              subtitle: TextStyle(
                color: subtitleColorDay,
                fontSize: 16,)
          ),
          appBarTheme: _appbarThemeDay);

  ThemeData get appThemeDataNight =>
      ThemeData(
          bottomAppBarColor: bottomNavBackgroundNight,
          primaryColorBrightness: Brightness.light,
          primaryColor: primaryColorDay,
          accentColor: accentColor,
          backgroundColor: backgroundNight,
          buttonColor: accentColor,
          errorColor: errorColorNight,
          disabledColor: disabledColorNight,
          toggleableActiveColor: primaryColorNight,
          fontFamily: 'Montserrat',
          iconTheme: const IconThemeData(color: Colors.black, size: iconSize),
          textTheme: const TextTheme(
              display1: TextStyle(
                  color: appBarTitleColorNight,
                  fontSize: 18,
                  fontWeight: FontWeight.w100),
              display4: TextStyle(
                  letterSpacing: 10,
                  color: appBarTitleColorNight,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
              title: TextStyle(
                  color: titleColorNight,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              subtitle: TextStyle(
                color: subtitleColorNight,
                fontSize: 16,)
          ),
          appBarTheme: _appbarThemeNight);
}
