import 'package:intl/intl.dart';

class Formatter {
  static String getFormattedAmount(double number){
    var formatter = NumberFormat()
      ..maximumFractionDigits = 2
      ..minimumExponentDigits = 2;
    return formatter.format(number);
  }
}