class HeroTags{
  static const String HERO_APP_TITLE = 'HERO_APP_TITLE';
  static const String HERO_COIN_ICON = 'HERO_COIN_ICON';
  static const String HERO_COIN_PRICE = 'HERO_COIN_PRICE';
  static const String HERO_COIN_CAP = 'HERO_COIN_CAP';
}