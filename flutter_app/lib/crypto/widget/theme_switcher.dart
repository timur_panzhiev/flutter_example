import 'package:flutter/material.dart';
import 'package:flutter_app/crypto/styles.dart';

class _CustomTheme extends InheritedWidget {
  final ThemeSwitcherState data;

  _CustomTheme({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_CustomTheme oldWidget) {
    return true;
  }
}

class ThemeSwitcher extends StatefulWidget {
  final Widget child;
  final bool isNightMode;

  const ThemeSwitcher({
    Key key,
    this.isNightMode,
    @required this.child,
  }) : super(key: key);

  @override
  ThemeSwitcherState createState() => new ThemeSwitcherState();

  static ThemeData of(BuildContext context) {
    _CustomTheme inherited =
    (context.inheritFromWidgetOfExactType(_CustomTheme) as _CustomTheme);
    return inherited.data.theme;
  }

  static ThemeSwitcherState instanceOf(BuildContext context) {
    _CustomTheme inherited = (context.inheritFromWidgetOfExactType(_CustomTheme) as _CustomTheme);
    return inherited.data;
  }
}

class ThemeSwitcherState extends State<ThemeSwitcher> {
  ThemeData _theme;

  ThemeData get theme => _theme;

  @override
  void initState() {
    _theme = getTheme(widget.isNightMode);
    super.initState();
  }
  
  getTheme(bool isNightMode) => 
      isNightMode ? Styles().appThemeDataNight : Styles().appThemeDataDay;


  void changeTheme(bool isNightMode) {
    setState(() {
      _theme = getTheme(isNightMode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return _CustomTheme(
      data: this,
      child: widget.child,
    );
  }
}