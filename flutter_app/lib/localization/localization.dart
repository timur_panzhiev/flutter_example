import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/localization/localization_strings.dart';

class S {
  final Locale locale;

  S(this.locale);

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'ru' : ru,
    'en' : en
  };

  _getValue(String key) => _localizedValues[locale.languageCode][key];

  //screen titles
  String get favouritesTitle => _getValue(FavoritesTitle);
  String get newsTitle => _getValue(NewsTitle);
  String get appTitle => _getValue(AppTitle);

  String get commonInfo => _getValue(CommonInfo);
  String get historicalData => _getValue(HistoricalData);
  String get exchanges => _getValue(Exchanges);
  String get news => _getValue(News);
}

class LocalizationDelegate extends LocalizationsDelegate<S> {
  const LocalizationDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<S> load(Locale locale) {
    return SynchronousFuture<S>(S(locale));
  }

  @override
  bool shouldReload(LocalizationDelegate old) => false;
}