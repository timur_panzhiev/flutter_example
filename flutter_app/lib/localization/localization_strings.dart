const String
AppTitle = 'app_title',
    FavoritesTitle = 'favorites_title',
    CommonInfo = 'common_info',
    HistoricalData = 'historical_data',
    Exchanges = 'exchanges',
    News = 'news',
    NewsTitle = 'news_title';

final ru = {
  AppTitle: 'Cryppy',
  CommonInfo: 'Инфо',
  HistoricalData: 'История',
  Exchanges: 'Биржи',
  News: 'Новости',
  FavoritesTitle: 'Избранные',
  NewsTitle: 'Новости'
};

final en = {
  AppTitle: 'Cryppy',
  CommonInfo: 'Common',
  HistoricalData: 'History',
  Exchanges: 'Exchanges',
  News: 'News',
  FavoritesTitle: 'Favorites',
  NewsTitle: 'News'
};
