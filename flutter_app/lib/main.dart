import 'package:flutter/material.dart';
import 'crypto/app_entry.dart';
import 'crypto/data/storage/storage.dart';
import 'package:flutter_app/crypto/widget/theme_switcher.dart';

void main() async {
  runApp(ThemeSwitcher(
      isNightMode: await Storage().isNightTheme() ?? false,
      child: CryptoApp())
  );
}